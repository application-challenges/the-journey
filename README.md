# The Journey
This project aims to build a sheet processor for a csv file. Very similar to what Google Sheets or 
Microsoft Excel do.

However, there are a few operations left that still need to be implemented due to time constraints. 
You can see in a section below what is developed and what is missing

## Allowed and/or unimplemented Operations
- [X] `^^` - Copies the formula from the cell above in the same column, with some special evaluation rules
- [X] `(A..Z)n` - references a cell by a combination of a column-letter+row-number. Ex: A2 B3
- [ ] `A^` - copies the evaluated result of the cell above in the same column
- [X] `!label` - Columns can have labels, which allows this ability to have different column groups in the same file as long as the number of columns stays consistent
- [X] `A^v` - copies the evaluated result of the last cell in the specified column from the most recently available column group that has data in that specified column
- [ ] `@label<n>` - References a specific labeled column and a specific row `n` under that column relative to where the column was labeled. This is a reference operator with relative row traversal
- [ ] `concat` - Concatenates all arguments as strings.
- [ ] `arrays,arrays,arrays` - Array types. Text divided by a coma `,`.
- [ ] `split` - Split text as per a divider.
- [ ] `spread` - Spread arrays.

Other operations might be added or implemented along the way.

## How to run it

### Prerequisites
* Install:
    * Go 1.20.* (other versions might work as well) - `brew install go`
    * Make - `brew install make`

(Note that this is for MacOS. Do the respective on Linux or Windows)

### Start to process
```
# Run locally test-transactions.csv file.
make start

# Run locally with another file
go run cmd/transaction_processor/main.go -file <ANOTHER_FILE>

# If you need to download go dependencies.
make mod
```

There are other useful commands for development purposes:
```
# Build the project.
make build

# Run tests and create coverage file.
make test
```

## Future improvements and TODOs
* Better error handling focused on file parsing/decoding files;
* Files can have only columns up to `Z`. Add more letters to the Column IDs;
* Clean the code a bit more. Break it into more functions so it can be read easily;
* Add/Implement more operations.