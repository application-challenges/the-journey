PROJECT_NAME=engineering-challenge
FOLDER_BIN=$(CURDIR)/bin
GO_TEST=go test ./...
GO_ENV=

start:
	$(GO_ENV) go run cmd/transaction_processor/main.go -file "./test-transactions.csv"

build:
	go build -o $(FOLDER_BIN)/$(PROJECT_NAME) $(CURDIR)/cmd/transaction_processor/main.go

test:
	$(GO_TEST) -coverprofile cover.out

mod:
	go mod tidy
