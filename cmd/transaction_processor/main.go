package main

import (
	"flag"
	"fmt"
	"os"

	"lab/engineering-challenge/internal/calc"
	"lab/engineering-challenge/internal/repository"
	"lab/engineering-challenge/internal/sheet"
	"lab/engineering-challenge/internal/transaction"

	"github.com/juju/errors"
)

func main() {
	filePath := flag.String("file", "./test-transactions.csv", "Path to transaction file")
	flag.Parse()

	fmt.Println("Welcome to Transaction Processor - Let's process", *filePath, "file!")

	parser := calc.NewExpressionParser()
	csvRepository := repository.NewCSVRepository(*filePath)
	decoder := sheet.NewDecoder(csvRepository, parser)

	processor := transaction.NewProcessor(*filePath, decoder, os.Stdout)

	err := processor.Process()
	if err != nil {
		if errors.Is(err, sheet.ErrParsingFile) {
			fmt.Println("File is not correct!")
			return
		}
		panic(errors.ErrorStack(err))
	}
}
