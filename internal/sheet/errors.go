package sheet

import "github.com/juju/errors"

var (
	// ErrInvalidTransactionType is returned when processor gets transactions in invalid types.
	ErrInvalidTransactionType = errors.New("sheet: transaction type is not valid")

	// ErrParsingFile is returned when it's not possible to parse the provided file.
	// Chances are this file is not well written or has a type in there.
	ErrParsingFile = errors.New("sheet: error while parsing provided file")
)
