package sheet

import (
	"regexp"
	"strconv"
	"strings"

	"lab/engineering-challenge/internal/calc"
	"lab/engineering-challenge/internal/repository"

	"github.com/juju/errors"
)

// ExpressionParser parses expressions.
type ExpressionParser interface {
	Parse(rawExpression string) calc.ASTNode
}

// Decoder decodes a CSV file.
type Decoder struct {
	repository repository.Repository
	parser     ExpressionParser
}

// NewDecoder instantiates a Decoder.
func NewDecoder(repository repository.Repository, parser ExpressionParser) *Decoder {
	return &Decoder{
		repository: repository,
		parser:     parser,
	}
}

// Decode decodes a CSV file into a sheet.
func (d *Decoder) Decode() (Sheet, error) {
	rawData, err := d.loadData()
	if err != nil {
		return nil, errors.Trace(err)
	}

	sheet, err := d.decode(rawData)
	if err != nil {
		return nil, err
	}

	err = linkCells(sheet)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return sheet, nil
}

func (d *Decoder) loadData() (string, error) {
	rawTransactions, err := d.repository.Get()
	if err != nil {
		return "", errors.Trace(err)
	}

	if _, ok := rawTransactions.(string); !ok {
		return "", ErrInvalidTransactionType
	}

	return rawTransactions.(string), nil
}

func (d *Decoder) decode(data string) (Sheet, error) {
	sheet := NewSheet(data)

	rawTransactions := strings.Split(data, "\n")
	for rowsIdx, tx := range rawTransactions {
		cells := strings.Split(tx, "|")

		for columnsIdx, rawCell := range cells {
			if strings.HasPrefix(rawCell, "!") {
				sheet[columnsIdx].Cells[rowsIdx] = NewLabelCell(rawCell, rawCell[1:])

				continue
			}

			if strings.HasPrefix(rawCell, "=") {
				rawCell = strings.Trim(rawCell, "=")

				if strings.Contains(rawCell, "^^") {
					sheet[columnsIdx].Cells[rowsIdx] = NewCellAbove(rawCell)

					continue
				}

				astNodes := d.parser.Parse(rawCell)

				cell, err := NewMultipleExpressionCell(astNodes)
				if err != nil {
					return nil, errors.Trace(err)
				}

				sheet[columnsIdx].Cells[rowsIdx] = cell

				continue
			}

			sheet[columnsIdx].Cells[rowsIdx] = &TextCell{val: rawCell}
		}
	}

	return sheet, nil
}

func linkCells(sheet Sheet) error {
	for i, column := range sheet {
		for j, cell := range column.Cells {
			switch c := cell.(type) {
			case *MultipleExpressionCell:
				err := populateCellForExpression(c.expr, sheet)
				if err != nil {
					return errors.Trace(err)
				}

				sheet[i].Cells[j] = cell
			case *CellAbove:
				c.Cell = &sheet[i].Cells[j-1]
				sheet[i].Cells[j] = c
			}
		}
	}

	return nil
}

func populateCellForExpression(exp calc.Expression, sheet Sheet) error {
	switch e := exp.(type) {
	case *CellExpression:
		columnID := letters.FindString(e.Value)
		column, ok := sheet.Column(columnID)
		if !ok {
			return nil
		}

		rowID := numbers.FindString(e.Value)
		if rowID == "" {
			e.Cell = getLastCell(column)

			return nil
		}

		row, err := strconv.Atoi(rowID)
		if err != nil {
			return errors.Trace(err)
		}

		e.Cell = &column.Cells[row-1]

		return nil
	case *calc.Binary:
		err := populateCellForExpression(e.X, sheet)
		if err != nil {
			return errors.Trace(err)
		}

		err = populateCellForExpression(e.Y, sheet)
		if err != nil {
			return errors.Trace(err)
		}
	case *calc.Comparison:
		err := populateCellForExpression(e.X, sheet)
		if err != nil {
			return errors.Trace(err)
		}

		err = populateCellForExpression(e.Y, sheet)
		if err != nil {
			return errors.Trace(err)
		}
	}

	return nil
}

func getLastCell(column Column) *Cell {
	var lastCell Cell
	for _, cell := range column.Cells {
		if cell == nil {
			continue
		}

		lastCell = cell
	}

	return &lastCell
}

var numbers = regexp.MustCompile("[0-9]+")
var letters = regexp.MustCompile("[A-Za-z]+")
