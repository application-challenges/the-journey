package sheet

import (
	"testing"

	"lab/engineering-challenge/internal/calc"

	"github.com/stretchr/testify/assert"
)

func Test_NewMultipleExpression(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		node := calc.ASTNode{
			Token: calc.Token{
				Type:  calc.OperatorType,
				Value: "+",
			},
			Arguments: []calc.ASTNode{
				{
					Token: calc.Token{
						Type:  calc.OperatorType,
						Value: "+",
					},
					Arguments: []calc.ASTNode{
						{
							Token: calc.Token{
								Type:  calc.LiteralType,
								Value: "1",
							},
						},
						{
							Token: calc.Token{
								Type:  calc.CellType,
								Value: "D4",
							},
						},
					},
				},
				{
					Token: calc.Token{
						Type:  calc.FunctionType,
						Value: "sum",
					},
					Arguments: []calc.ASTNode{
						{
							Token: calc.Token{
								Type:  calc.CellType,
								Value: "D5",
							},
						},
						{
							Token: calc.Token{
								Type:  calc.LiteralType,
								Value: "10",
							},
						},
					},
				},
			},
		}

		cell, err := NewMultipleExpressionCell(node)
		assert.Nil(t, err)
		assert.Equal(t, &MultipleExpressionCell{
			val: "",
			expr: &calc.Binary{
				Operation: '+',
				X: &calc.Binary{
					Operation: '+',
					X:         &calc.FloatExpression{Val: 1},
					Y: &CellExpression{
						Value: "D4",
						Cell:  nil,
					},
				},
				Y: &calc.Binary{
					Operation: '+',
					X: &CellExpression{
						Value: "D5",
						Cell:  nil,
					},
					Y: &calc.FloatExpression{Val: 10},
				},
			},
		}, cell)
	})
}

func Test_CellAbove(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		var cell Cell = NewTextCell("15")

		cellAbove := NewCellAbove("doesntmatter")
		cellAbove.Cell = &cell

		err := cellAbove.Evaluate()
		assert.Nil(t, err)
		assert.Equal(t, "15", cellAbove.Value())
	})
}
