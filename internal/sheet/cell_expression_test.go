package sheet

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_CellExpression(t *testing.T) {
	t.Run("success_evaluate", func(t *testing.T) {
		var cell Cell = NewTextCell("15")

		cellExpression := NewCellExpression("doesntmatter")
		cellExpression.Cell = &cell

		val, err := cellExpression.Evaluate()
		assert.Nil(t, err)
		assert.Equal(t, "15", val)
	})
}
