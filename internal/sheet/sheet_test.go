package sheet

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	a1Cell Cell = &TextCell{
		val: "hi",
	}
	sheet = Sheet{
		Column{
			ID: "A",
			Cells: []Cell{
				a1Cell,
				&TextCell{
					val: "bye",
				},
			},
		},
		Column{
			ID: "B",
			Cells: []Cell{
				&MultipleExpressionCell{
					expr: &CellExpression{
						Value: "A1",
						Cell:  &a1Cell,
					},
				},
				&CellAbove{
					Cell: &a1Cell,
				},
			},
		}, Column{
			ID: "C",
			Cells: []Cell{
				&TextCell{
					val: "extraColumn",
				},
			},
		},
	}
)

func TestSheet_Columns(t *testing.T) {
	num := sheet.Columns()
	assert.Equal(t, 3, num)
}

func TestSheet_Rows(t *testing.T) {
	num := sheet.Rows()
	assert.Equal(t, 2, num)
}

func TestSheet_Column(t *testing.T) {
	t.Run("exists", func(t *testing.T) {
		column, ok := sheet.Column("C")
		assert.True(t, ok)
		assert.Len(t, column.Cells, 1)
		assert.Equal(t, "C", column.ID)
		assert.Equal(t, &TextCell{
			val: "extraColumn",
		}, column.Cells[0])
	})

	t.Run("doesnt_exist", func(t *testing.T) {
		_, ok := sheet.Column("D")
		assert.False(t, ok)
	})
}

func TestNewSheet(t *testing.T) {
	rawSheet := `hi|=A1|extraColumn
bye|^^|`

	newSheet := NewSheet(rawSheet)
	assert.Equal(t, Sheet{
		Column{
			ID:    "A",
			Cells: make([]Cell, 2),
		},
		Column{
			ID:    "B",
			Cells: make([]Cell, 2),
		},
		Column{
			ID:    "C",
			Cells: make([]Cell, 2),
		},
	}, newSheet)
}

func TestSheet_Evaluate(t *testing.T) {
	err := sheet.Evaluate()
	assert.Nil(t, err)
	assert.Equal(t, "hi", sheet[1].Cells[0].Value())
	assert.Equal(t, "hi", sheet[1].Cells[1].Value())
}
