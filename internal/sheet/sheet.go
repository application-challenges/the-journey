package sheet

import (
	"strings"

	"github.com/juju/errors"
)

// Sheet represents a sheet (just like a Google Sheets sheet or a Microsoft
// Office sheet. It contains a list of Columns.
type Sheet []Column

// NewSheet instantiates a new Sheet.
func NewSheet(data string) Sheet {
	rawTransactions := strings.Split(data, "\n")
	columnsNum := len(strings.Split(rawTransactions[0], "|"))
	columns := make([]Column, columnsNum)

	for i := 0; i < columnsNum; i++ {
		column := columns[i]

		cells := make([]Cell, len(rawTransactions))
		column.Cells = append(column.Cells, cells...)

		// TODO: Fix this later:
		// IDs work only from A to Z. If we have more columns than that, it should be
		// something like this: "Z", "AA", "AB", "AC", etc.
		column.ID = string(rune(int('A') + i))

		columns[i] = column
	}

	return columns
}

// Column gets the Column by id.
func (s *Sheet) Column(id string) (Column, bool) {
	for _, column := range *s {
		if column.ID == id {
			return column, true
		}
	}
	return Column{}, false
}

// Rows return the number of rows on a Sheet.
func (s *Sheet) Rows() int {
	if len(*s) == 0 {
		return 0
	}

	return len((*s)[0].Cells)
}

// Columns return the number of columns on a Sheet.
func (s *Sheet) Columns() int {
	return len(*s)
}

// Evaluate evaluates/solves all cells inside a Sheet.
func (s *Sheet) Evaluate() error {
	for _, column := range *s {
		for _, cell := range column.Cells {
			if cell == nil {
				continue
			}
			err := cell.Evaluate()
			if err != nil {
				return errors.Trace(err)
			}
		}
	}

	return nil
}
