package sheet

import (
	"testing"

	"lab/engineering-challenge/internal/calc"

	"github.com/stretchr/testify/assert"
)

func TestDecoder_loadData(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		decoder := NewDecoder(&mockRepo{
			data: "hi|hello|hola\nbye|bye|bye",
		}, &mockParser{})

		data, err := decoder.loadData()
		assert.Nil(t, err)
		assert.Equal(t, "hi|hello|hola\nbye|bye|bye", data)
	})

	t.Run("error", func(t *testing.T) {
		decoder := NewDecoder(&mockRepo{
			err: assert.AnError,
		}, &mockParser{})

		_, err := decoder.loadData()
		assert.ErrorIs(t, err, assert.AnError)
	})
}

func TestDecoder_decode(t *testing.T) {
	rawString := `!a|=1
1|`
	decoder := NewDecoder(&mockRepo{}, &mockParser{})

	sheet, err := decoder.decode(rawString)
	assert.Nil(t, err)
	assert.Equal(t, Sheet{
		Column{
			ID: "A",
			Cells: []Cell{
				&LabelCell{
					val:   "!a",
					label: "a",
				},
				&TextCell{
					val: "1",
				},
			},
		},
		Column{
			ID: "B",
			Cells: []Cell{
				&MultipleExpressionCell{
					expr: &calc.FloatExpression{Val: 1},
				},
				&TextCell{
					val: "",
				},
			},
		},
	}, sheet)
}

func Test_linkCells(t *testing.T) {
	var sheet = Sheet{
		Column{
			ID: "A",
			Cells: []Cell{
				&TextCell{
					val: "hi",
				},
				&CellAbove{},
			},
		},
		Column{
			ID: "B",
			Cells: []Cell{
				&MultipleExpressionCell{
					expr: &CellExpression{
						Value: "A1",
					},
				},
			},
		},
	}

	err := linkCells(sheet)
	assert.Nil(t, err)
	assert.Equal(t, Sheet{
		Column{
			ID: "A",
			Cells: []Cell{
				&TextCell{
					val: "hi",
				},
				&CellAbove{
					Cell: &sheet[0].Cells[0],
				},
			},
		},
		Column{
			ID: "B",
			Cells: []Cell{
				&MultipleExpressionCell{
					expr: &CellExpression{
						Value: "A1",
						Cell:  &sheet[0].Cells[0],
					},
				},
			},
		},
	}, sheet)
}

type mockRepo struct {
	data any
	err  error
}

func (m *mockRepo) Get() (any, error) {
	return m.data, m.err
}

type mockParser struct{}

func (m *mockParser) Parse(rawExpression string) calc.ASTNode {
	return calc.ASTNode{
		Token: calc.Token{
			Type:  calc.LiteralType,
			Value: "1",
		},
	}
}
