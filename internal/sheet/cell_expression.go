package sheet

// CellExpression implements an Expression inside a Cell.
type CellExpression struct {
	Value string
	Cell  *Cell
}

// Evaluate evaluates/solves a CellExpression.
func (v *CellExpression) Evaluate() (string, error) {
	err := (*v.Cell).Evaluate()
	if err != nil {
		return "", err
	}

	return (*v.Cell).Value(), nil
}

// NewCellExpression instantiates a new CellExpression.
func NewCellExpression(rawValue string) *CellExpression {
	return &CellExpression{Value: rawValue}
}
