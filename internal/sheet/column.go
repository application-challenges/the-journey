package sheet

// Column represents a column of the sheet. It contains all Cells of that column.
// Some cells can be nil.
type Column struct {
	// ID that represents a Column. It's only letters. i.e. "A", "Z", "AB", "ZZZ".
	ID string
	// Cells are all the cells in the Column.
	Cells []Cell
}
