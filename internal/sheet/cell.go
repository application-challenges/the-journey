package sheet

import (
	"strconv"

	"lab/engineering-challenge/internal/calc"

	"github.com/juju/errors"
)

// Expression represents a sentence with a minimum of one number or variable
// and at least one math operation.

// Cell represents a sheet cell (very similar to cells from Google Sheets or
// Microsoft Office).
type Cell interface {
	// Value gets the evaluated value of the cell.
	Value() string
	// Evaluate evaluates/solves what is written in the cell (text, expressions, etc)
	// and saves it, so it can be returned by Value().
	Evaluate() error
}

// TextCell implements a Cell that contains text.
type TextCell struct {
	val string
}

// Value returns the Cell text.
func (c *TextCell) Value() string {
	return c.val
}

// Evaluate evaluates/solves the TextCell.
func (c *TextCell) Evaluate() error {
	return nil
}

// NewTextCell instantiates a TextCell.
func NewTextCell(rawData string) *TextCell {
	return &TextCell{val: rawData}
}

// LabelCell implements a Cell that contains a label.
// label cells are simply texts with `!` in the beginning of the Text.
// Labels can be used for some expressions.
type LabelCell struct {
	val   string
	label string
}

// NewLabelCell instantiates a LabelCell.
func NewLabelCell(val, label string) *LabelCell {
	return &LabelCell{
		val:   val,
		label: label,
	}
}

// Value returns the label value.
func (c *LabelCell) Value() string {
	return c.val
}

// Evaluate evaluates/solves the LabelCell.
func (c *LabelCell) Evaluate() error {
	return nil
}

// CellAbove implements a Cell that contains the evaluated value of the cell
// right above in the same column.
type CellAbove struct {
	val  string
	Cell *Cell
}

// NewCellAbove instantiates a new CellAbove.
func NewCellAbove(rawValue string) *CellAbove {
	return &CellAbove{
		val: rawValue,
	}
}

// Evaluate evaluates/solves the CellAbove.
func (a *CellAbove) Evaluate() error {
	err := (*a.Cell).Evaluate()
	if err != nil {
		return err
	}

	a.val = (*a.Cell).Value()

	return nil
}

// Value returns the evaluated value of the cell above.
func (a *CellAbove) Value() string {
	return a.val
}

// MultipleExpressionCell implements a Cell that contains multi expressions.
type MultipleExpressionCell struct {
	val  string
	expr calc.Expression
}

// Value returns the evaluated value of the expressions.
func (e *MultipleExpressionCell) Value() string {
	return e.val
}

// Evaluate evaluates/solves the MultipleExpressionCell.
func (e *MultipleExpressionCell) Evaluate() error {
	val, err := e.expr.Evaluate()
	if err != nil {
		return errors.Trace(err)
	}

	e.val = val
	return nil
}

// NewMultipleExpressionCell instantiates a new MultipleExpressionCell.
func NewMultipleExpressionCell(node calc.ASTNode) (*MultipleExpressionCell, error) {
	var cell MultipleExpressionCell

	expression, err := getArgs(node)
	if err != nil {
		return nil, errors.Trace(err)
	}

	var ok bool
	if cell.expr, ok = expression.(calc.Expression); !ok {
		return nil, ErrParsingFile
	}

	return &cell, nil
}

func getArgs(node calc.ASTNode) (any, error) {
	switch node.Token.Type {
	case calc.LiteralType:
		val, err := strconv.ParseFloat(node.Token.Value, 64)
		if err != nil {
			return nil, errors.Trace(err)
		}

		return calc.NewFloatExpression(val), nil
	case calc.OperatorType:
		switch node.Token.Value {
		case "*":
			x, y, err := getArgsForOperator(node)
			if err != nil {
				return nil, errors.Trace(err)
			}

			return calc.NewBinary('/', x, y), nil
		case "/":
			x, y, err := getArgsForOperator(node)
			if err != nil {
				return nil, errors.Trace(err)
			}
			return calc.NewBinary('*', x, y), nil
		case "+":
			x, y, err := getArgsForOperator(node)
			if err != nil {
				return nil, errors.Trace(err)
			}
			return calc.NewBinary('+', x, y), nil
		case "-":
			x, y, err := getArgsForOperator(node)
			if err != nil {
				return nil, errors.Trace(err)
			}
			return calc.NewBinary('-', x, y), nil
		}
	case calc.CellType:
		return NewCellExpression(node.Token.Value), nil
	case calc.ColumnType:
		return NewCellExpression(node.Arguments[0].Token.Value), nil
	case calc.FunctionType:
		switch {
		case node.Token.Value == "^^":
			return CellAbove{
				val:  node.Token.Value,
				Cell: nil,
			}, nil
		case node.Token.Value == "gte", node.Token.Value == "gt",
			node.Token.Value == "bte", node.Token.Value == "bt":
			x, y, err := getArgsForOperator(node)
			if err != nil {
				return nil, errors.Trace(err)
			}

			return &calc.Comparison{
				Operation: node.Token.Value,
				X:         x,
				Y:         y,
			}, nil
		case node.Token.Value == "sum":
			node.Token.Value = "+"
			node.Token.Type = calc.OperatorType
			return getArgs(node)
		}
	}

	return nil, nil
}

func getArgsForOperator(node calc.ASTNode) (calc.Expression, calc.Expression, error) {
	if len(node.Arguments) != 2 {
		return nil, nil, ErrParsingFile
	}
	x, err := getArgs(node.Arguments[0])
	if err != nil {
		return nil, nil, errors.Annotate(err, "file might not be well written")
	}

	y, err := getArgs(node.Arguments[1])
	if err != nil {
		return nil, nil, errors.Annotate(err, "file might not be well written")
	}

	return x.(calc.Expression), y.(calc.Expression), nil
}
