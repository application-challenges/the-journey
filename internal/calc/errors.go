package calc

import "github.com/juju/errors"

// ErrInvalidOperation is returned when the operation is not valid.
var ErrInvalidOperation = errors.New("calc: invalid operation")
