package calc

import (
	"regexp"
)

// ExpressionParser parses string expressions.
type ExpressionParser struct{}

// NewExpressionParser instantiates a new ExpressionParser.
func NewExpressionParser() *ExpressionParser {
	return &ExpressionParser{}
}

// Parse parses a string expression.
func (l *ExpressionParser) Parse(rawExpression string) ASTNode {
	tokens := l.tokenize(rawExpression)

	return l.parse(tokens)
}

// tokenize turns the raw expression into the respective ordered slice of tokens.
func (l *ExpressionParser) tokenize(expression string) []Token {
	var tokens []Token

	components := splitString(expression, func(r rune) bool {
		return r == '+' || r == '-' || r == '*' || r == ',' || r == '(' || r == ')' || r == '^'
	})

	for _, comp := range components {
		var t Token

		if comp == "sum" || comp == "spread" || comp == "split" || comp == "concat" ||
			comp == "gte" || comp == "gt" || comp == "bte" || comp == "bt" {
			t.Type = FunctionType
		} else if comp[0] >= 'A' && comp[0] <= 'Z' {
			t.Type = CellType
		} else if numbers.MatchString(comp) || letters.MatchString(comp) {
			t.Type = LiteralType
		} else if comp == "+" || comp == "-" || comp == "*" || comp == "/" {
			t.Type = OperatorType
		} else if comp == "(" {
			t.Type = OpenParenthesesType
		} else if comp == ")" {
			t.Type = CloseParenthesesType
		} else if comp == "," {
			t.Type = CommaType
		} else if comp == "^" {
			t.Type = ColumnType
		} else {
			continue
		}

		t.Value = comp
		tokens = append(tokens, t)
	}

	tokensLen := len(tokens)
	for i, t := range tokens {
		if t.Type == ColumnType && i < tokensLen-1 {
			t.Value = t.Value + tokens[i+1].Value
			tokens[i] = t

			tokens = append(tokens[:i+1], tokens[i+2:]...)
		}
	}

	return tokens
}

// Parse turns a slice of Tokens into an ASTNode.
func (l *ExpressionParser) parse(tokens []Token) ASTNode {
	var (
		currentNode = ASTNode{}
		stack       []ASTNode
	)

	for _, token := range tokens {
		switch token.Type {
		case FunctionType:
			if currentNode.Token == (Token{}) {
				currentNode.Token = token
				continue
			}
			node := ASTNode{
				Token: token,
			}
			stack = append(stack, currentNode)
			currentNode = node
		case OperatorType:
			if currentNode.Token == (Token{}) {
				currentNode.Token = token
				continue
			}
			node := ASTNode{
				Token:     token,
				Arguments: []ASTNode{currentNode},
			}
			currentNode = node
		case CellType, LiteralType:
			currentNode.Arguments = append(currentNode.Arguments, ASTNode{
				Token: token,
			})
		case ColumnType:
			if currentNode.Token == (Token{}) {
				currentNode.Token = token
				continue
			}
			lastArgIdx := len(currentNode.Arguments) - 1
			node := ASTNode{
				Token: token,
				Arguments: []ASTNode{
					currentNode.Arguments[lastArgIdx],
				},
			}
			currentNode.Arguments[lastArgIdx] = node
		case CommaType:
			funcNode := stack[len(stack)-1]
			if currentNode.Token == (Token{}) {
				funcNode.Arguments = append(funcNode.Arguments, currentNode.Arguments...)
			} else {
				funcNode.Arguments = append(funcNode.Arguments, currentNode)
			}
			stack[len(stack)-1] = funcNode
			currentNode = ASTNode{}
		case OpenParenthesesType:
			stack = append(stack, currentNode)
			currentNode = ASTNode{}
		case CloseParenthesesType:
			if len(stack) > 0 {
				funcNode := stack[len(stack)-1]
				stack = stack[:len(stack)-1]
				if currentNode.Token == (Token{}) {
					funcNode.Arguments = append(funcNode.Arguments, currentNode.Arguments...)
				} else {
					funcNode.Arguments = append(funcNode.Arguments, currentNode)
				}
				currentNode = funcNode
			}
		}
	}

	for {
		if len(stack) == 0 {
			break
		}

		node := stack[len(stack)-1]
		stack = stack[:len(stack)-1]

		node.Arguments = append(node.Arguments, currentNode)
		currentNode = node
	}

	return currentNode
}

func splitString(s string, f func(rune) bool) []string {
	var result []string
	current := ""
	for _, c := range s {
		if f(c) {
			if current != "" {
				result = append(result, current)
			}
			result = append(result, string(c))
			current = ""
		} else {
			current += string(c)
		}
	}
	if current != "" {
		result = append(result, current)
	}
	return result
}

var (
	// FunctionType represents a Token's function type.
	FunctionType TokenType = "Function"
	// LiteralType represents a Token's literal type.
	LiteralType TokenType = "Literal"
	// CellType represents a Token's cell type.
	CellType TokenType = "Cell"
	// OperatorType represents a Token's operator type.
	OperatorType TokenType = "Operator"
	// OpenParenthesesType represents a Token's open parentheses type.
	OpenParenthesesType TokenType = "OpenParentheses"
	// CloseParenthesesType represents a Token's close parentheses type.
	CloseParenthesesType TokenType = "CloseParentheses"
	// CommaType represents a Token's comma type.
	CommaType TokenType = "Comma"
	// ColumnType represents a Token's column type.
	ColumnType TokenType = "Column"

	numbers = regexp.MustCompile("[0-9]+")
	letters = regexp.MustCompile("[A-Za-z]+")
)

// TokenType represents a type of Token.
type TokenType string

// Token represents a single operator, function, literal, cell, etc.
type Token struct {
	Type TokenType
	// Value is the raw value of the Type.
	Value string
}

// ASTNode is a tree representation of the abstract syntactic structure of expressions.
// Each node of the tree denotes something occurring in the expression.
type ASTNode struct {
	Token     Token
	Arguments []ASTNode
}
