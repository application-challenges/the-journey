package calc

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMultipleExpression_Sum(t *testing.T) {
	// 3 + 10 + 10 * 3
	exp := Binary{
		Operation: '+',
		X: &Binary{
			Operation: '+',
			X:         &FloatExpression{Val: 3},
			Y:         &FloatExpression{Val: 10},
		},
		Y: &Binary{
			Operation: '*',
			X:         &FloatExpression{Val: 10},
			Y:         &FloatExpression{Val: 3},
		},
	}

	val, err := exp.Evaluate()
	assert.Nil(t, err)
	assert.Equal(t, "43.00", val)
}

func TestMultipleExpression_Comparison(t *testing.T) {
	t.Run("true_gte", func(t *testing.T) {
		exp := Comparison{
			Operation: "gte",
			X:         &FloatExpression{Val: 3},
			Y:         &FloatExpression{Val: 3},
		}

		val, err := exp.Evaluate()
		assert.Nil(t, err)
		assert.Equal(t, "true", val)
	})

	t.Run("false_gte", func(t *testing.T) {
		exp := Comparison{
			Operation: "gte",
			X:         &FloatExpression{Val: 2},
			Y:         &FloatExpression{Val: 3},
		}

		val, err := exp.Evaluate()
		assert.Nil(t, err)
		assert.Equal(t, "false", val)
	})

	t.Run("true_gt", func(t *testing.T) {
		exp := Comparison{
			Operation: "gt",
			X:         &FloatExpression{Val: 4},
			Y:         &FloatExpression{Val: 3},
		}

		val, err := exp.Evaluate()
		assert.Nil(t, err)
		assert.Equal(t, "true", val)
	})

	t.Run("false_gte", func(t *testing.T) {
		exp := Comparison{
			Operation: "gt",
			X:         &FloatExpression{Val: 3},
			Y:         &FloatExpression{Val: 3},
		}

		val, err := exp.Evaluate()
		assert.Nil(t, err)
		assert.Equal(t, "false", val)
	})

	t.Run("true_bte", func(t *testing.T) {
		exp := Comparison{
			Operation: "bte",
			X:         &FloatExpression{Val: 3},
			Y:         &FloatExpression{Val: 3},
		}

		val, err := exp.Evaluate()
		assert.Nil(t, err)
		assert.Equal(t, "true", val)
	})

	t.Run("false_bte", func(t *testing.T) {
		exp := Comparison{
			Operation: "bte",
			X:         &FloatExpression{Val: 4},
			Y:         &FloatExpression{Val: 3},
		}

		val, err := exp.Evaluate()
		assert.Nil(t, err)
		assert.Equal(t, "false", val)
	})

	t.Run("true_bt", func(t *testing.T) {
		exp := Comparison{
			Operation: "bt",
			X:         &FloatExpression{Val: 2},
			Y:         &FloatExpression{Val: 3},
		}

		val, err := exp.Evaluate()
		assert.Nil(t, err)
		assert.Equal(t, "true", val)
	})

	t.Run("false_bt", func(t *testing.T) {
		multipleExp := Comparison{
			Operation: "bt",
			X:         &FloatExpression{Val: 3},
			Y:         &FloatExpression{Val: 3},
		}

		val, err := multipleExp.Evaluate()
		assert.Nil(t, err)
		assert.Equal(t, "false", val)
	})
}
