package calc

import (
	"fmt"
	"strconv"

	"github.com/juju/errors"
)

// Expression represents a sentence with a minimum of one number or variable
// and at least one math operation.
type Expression interface {
	// Evaluate evaluates/solves the Expression.
	Evaluate() (string, error)
}

// FloatExpression implements an Expression that consists of one float variable.
type FloatExpression struct {
	Val float64
}

// Evaluate evaluates/solves a FloatExpression.
func (l *FloatExpression) Evaluate() (string, error) {
	return fmt.Sprintf("%f", l.Val), nil
}

// NewFloatExpression instantiates a FloatExpression.
func NewFloatExpression(rawValue float64) *FloatExpression {
	return &FloatExpression{Val: rawValue}
}

// Binary is a binary operator expression. e.g. +, -, * and /
type Binary struct {
	Operation rune
	X, Y      Expression
}

// Evaluate evaluates/solves a Binary expression.
func (b *Binary) Evaluate() (string, error) {
	x, y, err := evaluateValues(b.X, b.Y)
	if err != nil {
		return "", errors.Trace(err)
	}

	switch b.Operation {
	case '+':
		return fmt.Sprintf("%.2f", x+y), nil
	case '-':
		return fmt.Sprintf("%.2f", x-y), nil
	case '*':
		return fmt.Sprintf("%.2f", x*y), nil
	case '/':
		return fmt.Sprintf("%.2f", x/y), nil
	}

	return "", nil
}

// NewBinary instantiates a new Binary expression.
func NewBinary(operation rune, x, y Expression) *Binary {
	return &Binary{
		Operation: operation,
		X:         x,
		Y:         y,
	}
}

// Comparison is a comparison operator expression. e.g. gt, gte, bt, and bte
type Comparison struct {
	Operation string
	X, Y      Expression
}

// Evaluate evaluates/solves a Comparison expression.
func (c *Comparison) Evaluate() (string, error) {
	x, y, err := evaluateValues(c.X, c.Y)
	if err != nil {
		return "", errors.Trace(err)
	}
	switch c.Operation {
	case "gte":
		if x >= y {
			return "true", nil
		}

		return "false", nil
	case "gt":
		if x > y {
			return "true", nil
		}

		return "false", nil
	case "bte":
		if x <= y {
			return "true", nil
		}

		return "false", nil
	case "bt":
		if x < y {
			return "true", nil
		}

		return "false", nil
	}

	return "false", ErrInvalidOperation
}

func evaluateValues(xExp, yExp Expression) (float64, float64, error) {
	xStr, err := xExp.Evaluate()
	if err != nil {
		return 0, 0, errors.Trace(err)
	}

	yStr, err := yExp.Evaluate()
	if err != nil {
		return 0, 0, errors.Trace(err)
	}

	x, err := strconv.ParseFloat(xStr, 64)
	if err != nil {
		return 0, 0, errors.Trace(err)
	}

	y, err := strconv.ParseFloat(yStr, 64)
	if err != nil {
		return 0, 0, errors.Trace(err)
	}

	return x, y, nil
}
