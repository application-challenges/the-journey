package calc

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Lexer(t *testing.T) {
	t.Run("tokenize", func(t *testing.T) {
		lexer := NewExpressionParser()

		tokens := lexer.tokenize("1+D4+sum(D5,10)+A^v+10")
		assert.Equal(t, []Token{
			{
				Type:  LiteralType,
				Value: "1",
			},
			{
				Type:  OperatorType,
				Value: "+",
			},
			{
				Type:  CellType,
				Value: "D4",
			},
			{
				Type:  OperatorType,
				Value: "+",
			},
			{
				Type:  FunctionType,
				Value: "sum",
			},
			{
				Type:  OpenParenthesesType,
				Value: "(",
			},
			{
				Type:  CellType,
				Value: "D5",
			},
			{
				Type:  CommaType,
				Value: ",",
			},
			{
				Type:  LiteralType,
				Value: "10",
			},
			{
				Type:  CloseParenthesesType,
				Value: ")",
			},
			{
				Type:  OperatorType,
				Value: "+",
			},
			{
				Type:  CellType,
				Value: "A",
			},
			{
				Type:  ColumnType,
				Value: "^v",
			},
			{
				Type:  OperatorType,
				Value: "+",
			},
			{
				Type:  LiteralType,
				Value: "10",
			},
		}, tokens)
	})

	t.Run("success_multiple_function", func(t *testing.T) {
		lexer := NewExpressionParser()

		nodes := lexer.Parse("1+D4+sum(D5,10)+A^v-10")
		assert.Equal(t, ASTNode{
			Token: Token{
				Type:  OperatorType,
				Value: "+",
			},
			Arguments: []ASTNode{
				{
					Token: Token{
						Type:  OperatorType,
						Value: "+",
					},
					Arguments: []ASTNode{
						{
							Token: Token{
								Type:  LiteralType,
								Value: "1",
							},
						},
						{
							Token: Token{
								Type:  CellType,
								Value: "D4",
							},
						},
					},
				},
				{
					Token: Token{
						Type:  OperatorType,
						Value: "-",
					},
					Arguments: []ASTNode{
						{
							Token: Token{
								Type:  OperatorType,
								Value: "+",
							},
							Arguments: []ASTNode{
								{
									Token: Token{
										Type:  FunctionType,
										Value: "sum",
									},
									Arguments: []ASTNode{
										{
											Token: Token{
												Type:  CellType,
												Value: "D5",
											},
										},
										{
											Token: Token{
												Type:  LiteralType,
												Value: "10",
											},
										},
									},
								},
								{
									Token: Token{
										Type:  ColumnType,
										Value: "^v",
									},
									Arguments: []ASTNode{
										{
											Token: Token{
												Type:  CellType,
												Value: "A",
											},
										},
									},
								},
							},
						},
						{
							Token: Token{
								Type:  LiteralType,
								Value: "10",
							},
						},
					},
				},
			},
		}, nodes)
	})

	t.Run("success_last_column_cell", func(t *testing.T) {
		lexer := NewExpressionParser()

		nodes := lexer.Parse("A^v")
		assert.Equal(t, ASTNode{
			Token: Token{
				Type:  ColumnType,
				Value: "^v",
			},
			Arguments: []ASTNode{
				{
					Token: Token{
						Type:  CellType,
						Value: "A",
					},
				},
			},
		}, nodes)
	})

	t.Run("success_functions_cells_literals", func(t *testing.T) {
		lexer := NewExpressionParser()

		nodes := lexer.Parse("concat(15+10,D4,_hi_)")
		assert.Equal(t, ASTNode{
			Token: Token{
				Type:  FunctionType,
				Value: "concat",
			},
			Arguments: []ASTNode{
				{
					Token: Token{
						Type:  OperatorType,
						Value: "+",
					},
					Arguments: []ASTNode{
						{
							Token: Token{
								Type:  LiteralType,
								Value: "15",
							},
						},
						{
							Token: Token{
								Type:  LiteralType,
								Value: "10",
							},
						},
					},
				},
				{
					Token: Token{
						Type:  CellType,
						Value: "D4",
					},
				},
				{
					Token: Token{
						Type:  LiteralType,
						Value: "_hi_",
					},
				},
			},
		}, nodes)
	})
}
