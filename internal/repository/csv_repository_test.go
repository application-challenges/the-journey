package repository

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCSVRepository_Get(t *testing.T) {
	dir := "./"
	namePattern := "tmp_file"
	file, err := os.CreateTemp(dir, namePattern)
	if err != nil {
		t.Fatal(err)
	}

	text := "this is a text file\nsweeeet!"
	n, err := file.Write([]byte(text))
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, n, 28)

	repo := NewCSVRepository(file.Name())

	result, err := repo.Get()
	if err != nil {
		t.Fatal(err)
	}

	if _, ok := result.(string); !ok {
		t.Fail()
	}

	assert.Equal(t, text, result.(string))

	if err := os.Remove(file.Name()); err != nil {
		t.Fail()
	}
}
