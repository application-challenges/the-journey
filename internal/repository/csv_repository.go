package repository

import (
	"os"

	"github.com/juju/errors"
)

// CSVRepository implements a CSV Repository.
type CSVRepository struct {
	filePath string
}

// NewCSVRepository instantiates a CSVRepository.
func NewCSVRepository(filePath string) *CSVRepository {
	return &CSVRepository{
		filePath: filePath,
	}
}

// Get gets data from the CSV file.
func (c *CSVRepository) Get() (any, error) {
	content, err := os.ReadFile(c.filePath)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return string(content), nil
}
