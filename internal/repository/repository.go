package repository

// Repository is an abstraction of a repository of any type(s)
type Repository interface {
	Get() (any, error)
}
