package transaction

import (
	"io"

	"lab/engineering-challenge/internal/sheet"

	"github.com/juju/errors"
)

// Processor processes all transactions inside a CSV file.
type Processor struct {
	filePath string
	decoder  *sheet.Decoder
	writer   io.Writer
}

// NewProcessor instantiates a new Processor.
func NewProcessor(
	filePath string,
	decoder *sheet.Decoder,
	writer io.Writer,
) *Processor {
	return &Processor{
		filePath: filePath,
		decoder:  decoder,
		writer:   writer,
	}
}

// Process processes all transactions inside a csv file.
// In order to process, it needs to decode all data inside the file, evaluate it
// and print all evaluated cells.
func (p *Processor) Process() error {
	sheet, err := p.decoder.Decode()
	if err != nil {
		return errors.Trace(err)
	}

	err = sheet.Evaluate()
	if err != nil {
		return errors.Trace(err)
	}

	p.print(sheet)

	return nil
}

func (p *Processor) print(sheet sheet.Sheet) {
	for i := 0; i < sheet.Rows(); i++ {
		var payload []byte
		for j, column := range sheet {
			cell := column.Cells[i]
			var data []byte
			if cell != nil {
				data = []byte(cell.Value())
			}

			if j != 0 {
				data = append([]byte("|"), data...)
			}

			payload = append(payload, data...)
		}

		payload = append(payload, '\n')

		_, _ = p.writer.Write(payload)
	}
}
